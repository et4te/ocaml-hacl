{ pkgs, stdenv, opam2nix, fetchFromGitHub }:

with stdenv.lib;

stdenv.mkDerivation {
    name = "hacl";
    src = ../.;
    buildInputs = opam2nix.build {
      specs = opam2nix.toSpecs [
        "alcotest"
        "base"
        "bigstring"
        "dune"
	"hex"
        "ocplib-endian"
	"ocamlbuild"
	"ocamlfind"
        "stdio"
	"zarith"
      ];
      ocamlAttr = "ocaml";
      ocamlVersion = "4.06.1";
    };
    buildPhase = ''
      dune build
    '';
    installPhase = ''
      dune runtest
      cp -R _build $out/lib/hacl
    '';
}
